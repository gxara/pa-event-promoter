import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

var firebaseConfig = {
    apiKey: "AIzaSyBqWSPkiMr-4FWmxDotX3lz3fcuc2qoNOg",
    authDomain: "pa-event-promoter.firebaseapp.com",
    projectId: "pa-event-promoter",
    storageBucket: "pa-event-promoter.appspot.com",
    messagingSenderId: "405700650166",
    appId: "1:405700650166:web:25af33d6a81686f45b7217",
    measurementId: "G-XYWLD9YRJR"
};

firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();
export const firestore = firebase.firestore();
