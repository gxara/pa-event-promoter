import React from "react";
import { Link } from "react-router-dom";

const NavBar = () => {
  return (
    <div
      style={{
        width: "100%",
        height: "40px",
        backgroundColor: "#837ed7",
        display: "flex",
        color: "#fff",
        justifyContent: "space-around",
        alignItems: "center",
      }}
    >
      <Link
        style={{
          marginLeft: "35px",
          color: "#fff",
          fontSize: "16px",
          textDecoration: "none",
        }}
        to="/"
      >
        Inicio
      </Link>
      <Link
        style={{ color: "#fff", fontSize: "16px", textDecoration: "none" }}
        to="/eventos"
      >
        Eventos
      </Link>
      <Link
        style={{ color: "#fff", fontSize: "16px", textDecoration: "none" }}
        to="/sobre"
      >
        Sobre nós
      </Link>
    </div>
  );
};

export { NavBar };
