import axios from "axios";

import appConfig from "../config";

class EventsAPI {
  static requestWithAuth(url) {
    // TODO: Implementar autenticação
    return axios.get(url);
  }

  static postWithAuth(url, data) {
    // TODO: Implementar autenticação
    return axios.post(url, data);
  }

  static getAllEvents() {
    return EventsAPI.requestWithAuth(`${appConfig.eventPromoterApiUrl}/event`);
  }

  // Quebrada por precisar enviar o token
  static enrollUserToEvent(user, event, enrollment_option) {
    return EventsAPI.postWithAuth(
      `${appConfig.eventPromoterApiUrl}/enrollUser`,
      {
        user,
        event,
        enrollment_option,
      }
    );
  }
}

export { EventsAPI };
