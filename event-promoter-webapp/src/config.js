const config = {
  eventPromoterApiUrl:
    process.env.NODE_ENV === "production"
      ? "http://172.2.222.10:5000"
      : "http://localhost:5000",
  mapsKey: process.env.REACT_APP_MAPS_KEY,
};

export default config;
