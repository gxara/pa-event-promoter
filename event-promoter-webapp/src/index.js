import React from "react";
import ReactDOM from "react-dom";
import AppRouter from "./routes";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'

import "./index.css";

ReactDOM.render(
  <React.StrictMode>
    <div className="App">
      <AppRouter />
    </div>
  </React.StrictMode>,
  document.getElementById("root")
);
