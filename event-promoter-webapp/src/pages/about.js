import React, { cloneElement } from "react";
import "../styles/about_us.css";

const Profile = (props) => {
  return (
    <div className="event-promoter-developer-card">
      <img
        alt="profile-pic"
        width="150"
        style={{ borderRadius: "50%" }}
        src={props.pic}
      ></img>
      <div style={{ textAlign: "left", padding: 30 }}>
        <h3>{props.name}</h3>
        <label>
          Estudante de engenharia de computação na Universidade Federal do Rio
          de Janeiro
        </label>
      </div>
    </div>
  );
};
const AboutUs = () => {
  return (
    <div
      style={{ display: "flex", justifyContent: "center", flexWrap: "wrap" }}
    >
      <Profile pic="/assets/about_photo/cainapereira.jpeg" name="Cainã" />
      <Profile
        pic="/assets/about_photo/filipesilva.jfif"
        name="Filipe Augusto"
      />
      <Profile pic="/assets/about_photo/felipeassis.png" name="Felipe Assis" />
      <Profile
        pic="/assets/about_photo/fernandafonseca.jfif"
        name="Fernanda Fonseca"
      />
      <Profile pic="/assets/about_photo/gabrielxara.jfif" name="Gabriel Xará" />
      <Profile pic="/assets/about_photo/tuliolarena.jfif" name="Túlio" />
    </div>
  );
};

export default AboutUs;
