import React from "react";
import "../styles/landingPage.css";

const LandingPage = () => {
  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          width: "100%",
          padding: 80,
          alignItems: "center",
        }}
      >
        <img
          alt="event-promoter-app-logo"
          width="400"
          src="/assets/logo.png"
        ></img>
        <img width="400" src="/assets/dancing.webp" />
      </div>
      <h1>
        Somos o portal <u>número 1</u> para você encontrar e divulgar eventos.
      </h1>
    </div>
  );
};

export default LandingPage;
