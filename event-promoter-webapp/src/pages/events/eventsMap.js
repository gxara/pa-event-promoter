/* eslint react/prop-types: 0 */

import React, { Component } from "react";
import { Map, GoogleApiWrapper, InfoWindow, Marker } from "google-maps-react";
// import pos from "./pos.png";

const GoogleMap = (props) => {
  const { events } = props;

  const markers = events.map((event) => (
    <Marker
      position={{
        lat: event.event_address_lat,
        lng: event.event_address_long,
      }}
      title={event.event_name}
    >
      <InfoWindow visible={true}>
        <div>
          <p>
            Click on the map or drag the marker to select location where the
            incident occurred
          </p>
        </div>
      </InfoWindow>
    </Marker>
  ));

  return (
    <div style={{ textAlign: "center", width: "80%" }}>
      <Map
        google={props.google}
        style={{
          width: 0.3 * window.innerWidth,
          margin: "40px 0px 0px 40px",
          height: "50%",
          textAlign: "center",
        }}
        zoom={10}
        initialCenter={{
          lat: "-22.90780755960336",
          lng: "-43.20640988229842",
        }}
        apiKey="AIzaSyACenLm5l6r_nm1HR1SuoWax8ZwkT4bmcA"
      >
        {markers}
      </Map>
    </div>
  );
};

export default GoogleApiWrapper({
  apiKey: "AIzaSyACenLm5l6r_nm1HR1SuoWax8ZwkT4bmcA",
})(GoogleMap);

// export default GoogleMap
