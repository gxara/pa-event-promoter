import React, { useEffect, useState } from "react";
import { EventCard } from "./eventCard";
import { EventsAPI } from "../../services/eventService";
import MapContainer from "./eventsMap";
import { Input } from "antd";

import "../../styles/events.css";

const formatAddress = (patio, number, complement, postCode) => {
  return patio + ", " + number + " - " + complement + " - CEP: " + postCode;
};

const EventsList = () => {
  const [events, setEvents] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    refreshEventsList();
  }, []);

  const refreshEventsList = () => {
    EventsAPI.getAllEvents().then((response) => {
      setEvents(response.data);
    });
  };

  console.log("aaaa", search);

  return (
    <div>
      <h3 style={{ margin: 20 }}>
        Com certeza você vai achar um evento que é a sua cara!
      </h3>
      <Input
        style={{ width: 500 }}
        placeholder="Digite um termo para filtrar e exibir apenas eventos correspondentes"
        onChange={(v) => setSearch(v.target.value.toUpperCase())}
      />
      <div style={{ display: "flex", justifyContent: "space-around" }}>
        <div style={{ width: "30%", textAlign: "center", height: "500px" }}>
          <MapContainer
            events={events.filter((event) =>
              event.event_name.toUpperCase().includes(search)
            )}
          ></MapContainer>
        </div>

        <div
          style={{
            width: "70%",
            padding: "0px 30px",
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          {events
            .filter((event) => event.event_name.toUpperCase().includes(search))
            .map((event) => {
              return (
                <EventCard
                  title={event.event_name}
                  description={event.event_description}
                  type={event.category_description}
                  date={event.event_date}
                  members={event.num_participantes}
                  eventId={event.event_id}
                  address={formatAddress(
                    event.event_address_patio,
                    event.event_address_number,
                    event.event_address_complement,
                    event.event_address_postcode
                  )}
                  pic={event.photo_path}
                  onUpdate={refreshEventsList}
                />
              );
            })}
        </div>
        {/* <EventCard title="Feira da Carioca" pic="/assets/eventos1.jpg" /> */}
        {/* <EventCard title="Casamento da Juliana" pic="/assets/eventos2.jpg" />
        <EventCard title="Formatura de Engenharia" pic="/assets/eventos3.jpg" />
        <EventCard title="Calourada da Odonto" pic="/assets/eventos4.jpg" />
        <EventCard title="Reunião de artistas" pic="/assets/eventos5.jpg" />
        <EventCard title="Formatura de Engenharia" pic="/assets/eventos3.jpg" />
        <EventCard title="Calourada da Odonto" pic="/assets/eventos4.jpg" />
        <EventCard title="Reunião de artistas" pic="/assets/eventos5.jpg" /> */}
      </div>
    </div>
  );
};

export default EventsList;
