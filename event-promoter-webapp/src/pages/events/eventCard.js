import React, { useState } from "react";
import { Statistic, Modal, Button } from "antd";
import { EventsAPI } from "../../services/eventService";
import {
  FacebookIcon,
  TelegramIcon,
  TwitterIcon,
  WhatsappIcon,
} from "react-share";
import "../../styles/events.css";
import MapContainer from "./eventsMap";

const EventCard = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const enrollEvent = () => {
    EventsAPI.enrollUserToEvent(1, props.eventId, 1).then(props.onUpdate());
  };

  return (
    <div className="event-card">
      <>
        <Modal
          title={props.title}
          cancelText="Fechar"
          visible={isModalVisible}
          footer={null}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <h3>Descrição</h3>
          <p>{props.description}</p>
          <h3>Data</h3>
          <p>{new Date(props.date).toLocaleDateString("pt-BR")}</p>
          <h3>Endereço</h3>
          <p>{props.address}</p>

          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
              marginTop: 55,
            }}
          >
            <div style={{ textAlign: "left" }}>
              <h4>Participar</h4>

              <Button
                type="primary"
                style={{ margin: 10 }}
                onClick={enrollEvent}
              >
                Quero me inscrever
              </Button>
            </div>
            <div style={{ textAlign: "right" }}>
              <h4>Compartilhar</h4>
              <WhatsappIcon size={36} style={{ margin: 5 }} />
              <FacebookIcon size={36} style={{ margin: 5 }} />
              <TelegramIcon size={36} style={{ margin: 5 }} />
              <TwitterIcon size={36} style={{ margin: 5 }} />
            </div>
          </div>
        </Modal>
      </>
      <img alt="event" src={props.pic} width="250px" height="170px"></img>
      <h3 style={{ margin: 10 }}>{props.title}</h3>
      <div style={{ width: "100%" }}>
        <Statistic title="Participantes" value={props.members} />
        <div
          style={{
            display: "flex",
            margin: 20,
            alignItems: "baseline",
            justifyContent: "space-around",
          }}
        >
          <p>{new Date(props.date).toLocaleDateString("pt-BR")}</p>
          <Button type="primary" size="small" onClick={showModal}>
            Ver mais
          </Button>
        </div>
      </div>
    </div>
  );
};

export { EventCard };
