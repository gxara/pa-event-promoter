import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import LandingPage from "./pages/landingPage";
import EventsList from "./pages/events/eventsList";
import AboutUs from "./pages/about";
import { NavBar } from "./components/Navbar";

const AppRouter = () => {
  return (
    <Router>
      <NavBar />
      <Switch>
        <Route path="/eventos">
          <EventsList />
        </Route>
        <Route path="/sobre">
          <AboutUs />
        </Route>
        <Route path="/">
          <LandingPage />
        </Route>
      </Switch>
    </Router>
  );
};

export default AppRouter;
