# pa-event-promoter

Este projeto foi desenvolvido para a disciplina de Programação Avançada do curso de Engenharia de Computação e Informação da UFRJ. A proposta envolve a criação de um MVP para resolver um problema real encontrado pela equipe. Tal aplicação foi desenvolvida em forma de _Single-Page Application_ (SPA) e utiliza contêineres Docker, procurando seguir os princípios ideals e _Domain-Driven Design_ (DDD).

# MVP:
- [Relatório](utils/Relat%C3%B3rio%20-%20MVP.pdf)
- [Slides - 1ª Entrega](utils/Slides%20-%20MVP.pdf)
- [Demonstração - 1ª Entrega](utils/Demonstra%C3%A7%C3%A3o.mp4)


# Descrição dos contêineres:

## event-promoter-api

Contêiner da API REST desenvolvida com Node.js e o framework express.

## event-promoter-webapp

Contêiner do _front-end_ da aplicação em React.

## event-promoter-db

Contêiner do banco de dados PostgreSQL - responsável por servir toda a aplicação.

## pgadmin

Contêiner da plataforma de administração e desenvolvimento do PostgreSQL
