/* Visibilidade */
INSERT INTO public.tbl_visibility VALUES (1, 'Public');
INSERT INTO public.tbl_visibility VALUES (2, 'Private');
INSERT INTO public.tbl_visibility VALUES (3, 'Unlisted');

/* Permissões do evento */
INSERT INTO public.tbl_permission (permission_description) values ('Owner');

/* Usuários */
INSERT INTO public.tbl_user (user_name, user_surname, user_birthdate) VALUES('Filipe', 'Silva', '1999-09-02');
INSERT INTO public.tbl_user (user_name, user_surname, user_birthdate) VALUES('Fernanda', 'Fonseca', '1999-10-04');
INSERT INTO public.tbl_user (user_name, user_surname, user_birthdate, user_email, user_uid) VALUES('Fulano', 'Silva', '2000-01-02', 'teste@gmail.com', 'mZ52skJENVVeMJY9PhlPOE1QHsP2');


/* Email */
-- INSERT INTO public.tbl_email (email_address, email_created_at, user_id) VALUES('dfilipeaugusto@poli.ufrj.br', '2021-05-15 15:41:37.615', 1);

/* Telefone */
INSERT INTO public.tbl_phone (phone_number, user_id) VALUES('11974658453', 1);

/* Categoria */
INSERT INTO public.tbl_category (category_description) VALUES('Party');
INSERT INTO public.tbl_category (category_description) VALUES('Fair');
INSERT INTO public.tbl_category (category_description, parent_category_id) VALUES('Arts and Crafts', 2);
INSERT INTO public.tbl_category (category_description, parent_category_id) VALUES('Wedding', NULL);

/* Estado do evento */
INSERT INTO public.tbl_status_event (status_event_description) values ('Confirmed');
INSERT INTO public.tbl_status_event (status_event_description) values ('Review');
INSERT INTO public.tbl_status_event (status_event_description) values ('Canceled');

/* Enrollment Options */
INSERT INTO public.tbl_enrollment_option (enrollment_option_description, enrollment_option_created_at, enrollment_option_ended_at) VALUES('Yes', '2021-05-16 00:41:35.923', NULL);
INSERT INTO public.tbl_enrollment_option (enrollment_option_description, enrollment_option_created_at, enrollment_option_ended_at) VALUES('Maybe', '2021-05-16 00:41:35.928', NULL);
INSERT INTO public.tbl_enrollment_option (enrollment_option_description, enrollment_option_created_at, enrollment_option_ended_at) VALUES('No', '2021-05-16 00:41:35.931', NULL);


/* Evento */
INSERT INTO public.tbl_event (event_name, event_description, event_date, event_created_at, event_address_patio, event_address_number, event_address_complement, event_address_postcode, event_address_lat, event_address_long, status_event_id, visibility_id, category_id) VALUES('Aniversário do Fulano', 'Tá chegando a hora! Fulano está ficando mais experiente', now() + interval '25' day, '2021-05-15 19:37:22.137', NULL, NULL, NULL, '20511000', '-22.92213719423369', '-43.23535385324578', 1, 2, 1);
INSERT INTO public.tbl_event (event_name, event_description, event_date, event_created_at, event_address_patio, event_address_number, event_address_complement, event_address_postcode, event_address_lat, event_address_long, status_event_id, visibility_id, category_id) VALUES('Raízes da Terra', 'Venha apreciar o trabalho de artesãos nômades em Paraty', '2021-10-04 05:30:22.137', '2021-05-16 22:35:40.031', 'Estrada da Colina', 3043, '', '23970000', '-23.211930971316928', '-44.71839841441237', 1, 1, 3);
INSERT INTO public.tbl_event (event_name, event_description, event_date, event_created_at, event_address_patio, event_address_number, event_address_complement, event_address_postcode, event_address_lat, event_address_long, status_event_id, visibility_id, category_id) VALUES('Casamento da Juliana', 'Nosso presente é a sua presença', '2021-07-05 14:00:00.000', '2021-05-17 01:04:22.472', 'Estrada da Bica', 12, NULL, '21931072', '-22.818625023847687', '-43.194173075847466', 1, 2, 4);
INSERT INTO public.tbl_event (event_name, event_description, event_date, event_created_at, event_address_patio, event_address_number, event_address_complement, event_address_postcode, event_address_lat, event_address_long, status_event_id, visibility_id, category_id) VALUES('Feira da Carioca', 'Venha aproveitar a nossa festa', '2021-11-12 21:30:00.000', '2021-05-17 01:01:01.095', NULL, NULL, NULL, '20040060', '-22.90815970160956', '-43.17788697383024', 1, 1, 1);
INSERT INTO public.tbl_event (event_name, event_description, event_date, event_created_at, event_address_patio, event_address_number, event_address_complement, event_address_postcode, event_address_lat, event_address_long, status_event_id, visibility_id, category_id) VALUES('Feira de arte de Botafogo', 'Encontro de todos aqueles que produzem ou apreciam arte e artesanato', '2021-08-09 09:30:00.000', '2021-05-17 01:08:39.357', NULL, NULL, NULL, '22250040', '-22.947611025853107', '-43.18318303833473', 1, 3, 1);
INSERT INTO public.tbl_event (event_name, event_description, event_date, event_created_at, event_address_patio, event_address_number, event_address_complement, event_address_postcode, event_address_lat, event_address_long, status_event_id, visibility_id, category_id) VALUES('Calourada da Odonto', 'Bora para a maior calourada, bixo! ', '2021-08-09 09:30:00.000', '2021-05-17 01:08:39.357', NULL, NULL, NULL, '21941598', '-22.846772102733787', '-43.237907540321544', 1, 3, 1);

/* Inscrição de participante */
INSERT INTO public.tbl_enrollment (enrollment_created_at, user_id, event_id, enrollment_option_id) VALUES('2021-05-16 22:35:43.125', 2, 2, 1);
INSERT INTO public.tbl_enrollment (enrollment_created_at, user_id, event_id, enrollment_option_id) VALUES('2021-05-17 01:15:30.316', 1, 4, 2);

/* Adicionando foto ao evento */
INSERT INTO public.tbl_photo (photo_path, photo_created_at, photo_position, event_id) VALUES ('/assets/eventos1.jpg', '2021-05-16 22:39:29.350', 0, 1);
INSERT INTO public.tbl_photo (photo_path, photo_created_at, photo_position, event_id) VALUES ('/assets/eventos2.jpg', '2021-05-16 22:39:29.350', 0, 2);
INSERT INTO public.tbl_photo (photo_path, photo_created_at, photo_position, event_id) VALUES ('/assets/eventos3.jpg', '2021-05-16 22:39:29.350', 0, 3);
INSERT INTO public.tbl_photo (photo_path, photo_created_at, photo_position, event_id) VALUES ('/assets/eventos4.jpg', '2021-05-16 22:39:29.350', 0, 4);
INSERT INTO public.tbl_photo (photo_path, photo_created_at, photo_position, event_id) VALUES ('/assets/eventos5.jpg', '2021-05-16 22:39:29.350', 0, 5);
INSERT INTO public.tbl_photo (photo_path, photo_created_at, photo_position, event_id) VALUES ('/assets/eventos6.jpg', '2021-05-16 22:39:29.350', 0, 6);

/* Inserções temporárias (Mudar os valores para fazerem mais sentido) */
INSERT INTO tbl_keyword (keyword_description) VALUES ('keyword1');
INSERT INTO tbl_keyword (keyword_description) VALUES ('keyword2');
INSERT INTO tbl_keyword (keyword_description) VALUES ('keyword3');
INSERT INTO tbl_event_keyword (event_id, keyword_id) VALUES (1,1);
INSERT INTO tbl_event_keyword (event_id, keyword_id) VALUES (1,3);
INSERT INTO tbl_event_keyword (event_id, keyword_id) VALUES (3,1);
INSERT INTO tbl_event_keyword (event_id, keyword_id) VALUES (5,2);
INSERT INTO tbl_status_promoter (status_promoter_description) VALUES ('status1');
INSERT INTO tbl_status_promoter (status_promoter_description) VALUES ('status2');
INSERT INTO tbl_status_promoter (status_promoter_description) VALUES ('status3');
INSERT INTO tbl_promoter (promoter_name,promoter_description,user_id,status_promoter_id) VALUES ('Promoter1 LTDA','Há 10 anos no mercado',1,1);
INSERT INTO tbl_promoter (promoter_name,promoter_description,user_id,status_promoter_id) VALUES ('Promoter2 S/A','Há 20 anos no mercado',1,2);
INSERT INTO tbl_promoter (promoter_name,promoter_description,user_id,status_promoter_id) VALUES ('Promoter3 S/A','Há 30 anos no mercado',2,3);
INSERT INTO tbl_currency (currency_description) VALUES ('real');
INSERT INTO tbl_currency (currency_description) VALUES ('dólar');
INSERT INTO tbl_currency (currency_description) VALUES ('euro');
INSERT INTO tbl_item (item_name,item_description,item_price,promoter_id,event_id,currency_id) VALUES ('item1','desc1',10,3,2,1);
INSERT INTO tbl_item (item_name,item_description,item_price,promoter_id,event_id,currency_id) VALUES ('item2','desc2',177,3,2,1);
INSERT INTO tbl_item (item_name,item_description,item_price,promoter_id,event_id,currency_id) VALUES ('item3','desc3',15,1,1,1);
INSERT INTO tbl_event_user (event_user_created_at, user_id, event_id, permission_id) VALUES (now(),1,4,1);