
CREATE SEQUENCE public.tbl_enrollment_option_enrollment_option_id_seq_1;

CREATE TABLE public.tbl_enrollment_option (
                enrollment_option_id INTEGER NOT NULL DEFAULT nextval('public.tbl_enrollment_option_enrollment_option_id_seq_1'),
                enrollment_option_description VARCHAR NOT NULL,
                enrollment_option_created_at TIMESTAMP NOT NULL,
                enrollment_option_ended_at TIMESTAMP,
                CONSTRAINT enrollment_option_id PRIMARY KEY (enrollment_option_id)
);


ALTER SEQUENCE public.tbl_enrollment_option_enrollment_option_id_seq_1 OWNED BY public.tbl_enrollment_option.enrollment_option_id;

CREATE SEQUENCE public.tbl_keyword_keyword_id_seq;

CREATE TABLE public.tbl_keyword (
                keyword_id INTEGER NOT NULL DEFAULT nextval('public.tbl_keyword_keyword_id_seq'),
                keyword_description VARCHAR NOT NULL,
                CONSTRAINT keyword_id PRIMARY KEY (keyword_id)
);


ALTER SEQUENCE public.tbl_keyword_keyword_id_seq OWNED BY public.tbl_keyword.keyword_id;

CREATE SEQUENCE public.tbl_category_category_id_seq_1;

CREATE TABLE public.tbl_category (
                category_id INTEGER NOT NULL DEFAULT nextval('public.tbl_category_category_id_seq_1'),
                category_description VARCHAR NOT NULL,
                Parent_category_id INTEGER,
                CONSTRAINT category_pk PRIMARY KEY (category_id)
);


ALTER SEQUENCE public.tbl_category_category_id_seq_1 OWNED BY public.tbl_category.category_id;

CREATE SEQUENCE public.tbl_visibility_visibility_id_seq_1;

CREATE TABLE public.tbl_visibility (
                visibility_id INTEGER NOT NULL DEFAULT nextval('public.tbl_visibility_visibility_id_seq_1'),
                visibility_description VARCHAR NOT NULL,
                CONSTRAINT visibility_id PRIMARY KEY (visibility_id)
);


ALTER SEQUENCE public.tbl_visibility_visibility_id_seq_1 OWNED BY public.tbl_visibility.visibility_id;

CREATE SEQUENCE public.tbl_status_promoter_status_promoter_id_seq_1;

CREATE TABLE public.tbl_status_promoter (
                status_promoter_id INTEGER NOT NULL DEFAULT nextval('public.tbl_status_promoter_status_promoter_id_seq_1'),
                status_promoter_description VARCHAR NOT NULL,
                CONSTRAINT status_promoter_id PRIMARY KEY (status_promoter_id)
);


ALTER SEQUENCE public.tbl_status_promoter_status_promoter_id_seq_1 OWNED BY public.tbl_status_promoter.status_promoter_id;

CREATE SEQUENCE public.tbl_status_event_status_event_id_seq_1;

CREATE TABLE public.tbl_status_event (
                status_event_id INTEGER NOT NULL DEFAULT nextval('public.tbl_status_event_status_event_id_seq_1'),
                status_event_description VARCHAR NOT NULL,
                CONSTRAINT status_event_id PRIMARY KEY (status_event_id)
);


ALTER SEQUENCE public.tbl_status_event_status_event_id_seq_1 OWNED BY public.tbl_status_event.status_event_id;

CREATE SEQUENCE public.tbl_currency_currency_id_seq_1_1;

CREATE TABLE public.tbl_currency (
                currency_id INTEGER NOT NULL DEFAULT nextval('public.tbl_currency_currency_id_seq_1_1'),
                currency_description VARCHAR NOT NULL,
                CONSTRAINT currency_id PRIMARY KEY (currency_id)
);


ALTER SEQUENCE public.tbl_currency_currency_id_seq_1_1 OWNED BY public.tbl_currency.currency_id;

CREATE SEQUENCE public.tbl_permission_permission_id_seq_1;

CREATE TABLE public.tbl_permission (
                permission_id INTEGER NOT NULL DEFAULT nextval('public.tbl_permission_permission_id_seq_1'),
                permission_description VARCHAR NOT NULL,
                CONSTRAINT permission_id PRIMARY KEY (permission_id)
);


ALTER SEQUENCE public.tbl_permission_permission_id_seq_1 OWNED BY public.tbl_permission.permission_id;

CREATE SEQUENCE public.tbl_event_event_id_seq;

CREATE TABLE public.tbl_event (
                event_id INTEGER NOT NULL DEFAULT nextval('public.tbl_event_event_id_seq'),
                event_name VARCHAR NOT NULL,
                event_description VARCHAR NOT NULL,
                event_date TIMESTAMP NOT NULL,
                event_created_at TIMESTAMP NOT NULL,
                event_address_patio VARCHAR,
                event_address_number INTEGER,
                event_address_complement VARCHAR,
                event_address_postcode VARCHAR,
                event_address_lat VARCHAR,
                event_address_long VARCHAR,
                status_event_id INTEGER NOT NULL,
                visibility_id INTEGER NOT NULL,
                category_id INTEGER NOT NULL,
                CONSTRAINT event_id_pk PRIMARY KEY (event_id)
);


ALTER SEQUENCE public.tbl_event_event_id_seq OWNED BY public.tbl_event.event_id;

CREATE SEQUENCE public.tbl_photo_photo_id_seq;

CREATE TABLE public.tbl_photo (
                photo_id INTEGER NOT NULL DEFAULT nextval('public.tbl_photo_photo_id_seq'),
                photo_path VARCHAR NOT NULL,
                photo_created_at TIMESTAMP NOT NULL,
                photo_position INTEGER NOT NULL,
                event_id INTEGER NOT NULL,
                CONSTRAINT photo_id PRIMARY KEY (photo_id)
);


ALTER SEQUENCE public.tbl_photo_photo_id_seq OWNED BY public.tbl_photo.photo_id;

CREATE UNIQUE INDEX tbl_photo_position
 ON public.tbl_photo
 ( photo_position, event_id );

CREATE SEQUENCE public.tbl_event_keyword_event_keyword_id_seq;

CREATE TABLE public.tbl_event_keyword (
                event_keyword_id INTEGER NOT NULL DEFAULT nextval('public.tbl_event_keyword_event_keyword_id_seq'),
                event_id INTEGER NOT NULL,
                keyword_id INTEGER NOT NULL,
                CONSTRAINT event_keyword_id PRIMARY KEY (event_keyword_id)
);


ALTER SEQUENCE public.tbl_event_keyword_event_keyword_id_seq OWNED BY public.tbl_event_keyword.event_keyword_id;

CREATE SEQUENCE public.tbl_user_user_id_seq;

CREATE TABLE public.tbl_user (
                user_id INTEGER NOT NULL DEFAULT nextval('public.tbl_user_user_id_seq'),
                user_name VARCHAR NOT NULL,
                user_surname VARCHAR NOT NULL,
                user_birthdate DATE NOT NULL,
                user_email VARCHAR,
                user_uid VARCHAR,
                CONSTRAINT user_id PRIMARY KEY (user_id)
);


ALTER SEQUENCE public.tbl_user_user_id_seq OWNED BY public.tbl_user.user_id;

CREATE UNIQUE INDEX tbl_user_uid
 ON public.tbl_user
 ( user_uid );

CREATE INDEX tbl_user_uid_hash
 ON public.tbl_user USING HASH
 ( user_uid );

CREATE SEQUENCE public.tbl_enrollment_enrollment_id_seq;

CREATE TABLE public.tbl_enrollment (
                enrollment_id INTEGER NOT NULL DEFAULT nextval('public.tbl_enrollment_enrollment_id_seq'),
                enrollment_created_at TIMESTAMP NOT NULL,
                user_id INTEGER NOT NULL,
                event_id INTEGER NOT NULL,
                enrollment_option_id INTEGER NOT NULL,
                CONSTRAINT enrollment_id PRIMARY KEY (enrollment_id)
);


ALTER SEQUENCE public.tbl_enrollment_enrollment_id_seq OWNED BY public.tbl_enrollment.enrollment_id;

CREATE SEQUENCE public.tbl_phone_phone_id_seq;

CREATE TABLE public.tbl_phone (
                phone_id INTEGER NOT NULL DEFAULT nextval('public.tbl_phone_phone_id_seq'),
                phone_number VARCHAR NOT NULL,
                user_id INTEGER NOT NULL,
                CONSTRAINT phone_id PRIMARY KEY (phone_id)
);


ALTER SEQUENCE public.tbl_phone_phone_id_seq OWNED BY public.tbl_phone.phone_id;

CREATE SEQUENCE public.tbl_promoter_promoter_id_seq_1;

CREATE TABLE public.tbl_promoter (
                promoter_id INTEGER NOT NULL DEFAULT nextval('public.tbl_promoter_promoter_id_seq_1'),
                promoter_name VARCHAR NOT NULL,
                promoter_description VARCHAR NOT NULL,
                user_id INTEGER NOT NULL,
                status_promoter_id INTEGER NOT NULL,
                CONSTRAINT promoter_id PRIMARY KEY (promoter_id)
);


ALTER SEQUENCE public.tbl_promoter_promoter_id_seq_1 OWNED BY public.tbl_promoter.promoter_id;

CREATE SEQUENCE public.tbl_item_item_id_seq;

CREATE TABLE public.tbl_item (
                item_id INTEGER NOT NULL DEFAULT nextval('public.tbl_item_item_id_seq'),
                item_name VARCHAR NOT NULL,
                item_description VARCHAR NOT NULL,
                item_price NUMERIC,
                promoter_id INTEGER NOT NULL,
                event_id INTEGER NOT NULL,
                currency_id INTEGER,
                CONSTRAINT item_id PRIMARY KEY (item_id)
);


ALTER SEQUENCE public.tbl_item_item_id_seq OWNED BY public.tbl_item.item_id;

CREATE SEQUENCE public.tbl_event_user_event_user_id_seq;

CREATE TABLE public.tbl_event_user (
                event_user_id INTEGER NOT NULL DEFAULT nextval('public.tbl_event_user_event_user_id_seq'),
                event_user_created_at TIMESTAMP NOT NULL,
                user_id INTEGER NOT NULL,
                event_id INTEGER NOT NULL,
                permission_id INTEGER NOT NULL,
                CONSTRAINT event_user_id PRIMARY KEY (event_user_id)
);


ALTER SEQUENCE public.tbl_event_user_event_user_id_seq OWNED BY public.tbl_event_user.event_user_id;

CREATE INDEX tbl_event_id
 ON public.tbl_event_user USING HASH
 ( event_id );

ALTER TABLE public.tbl_enrollment ADD CONSTRAINT tbl_enrollment_option_tbl_enrollment_fk
FOREIGN KEY (enrollment_option_id)
REFERENCES public.tbl_enrollment_option (enrollment_option_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_event_keyword ADD CONSTRAINT tbl_keyword_tbl_event_keyword_fk
FOREIGN KEY (keyword_id)
REFERENCES public.tbl_keyword (keyword_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_event ADD CONSTRAINT tbl_category_tbl_event_fk
FOREIGN KEY (category_id)
REFERENCES public.tbl_category (category_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_category ADD CONSTRAINT tbl_category_tbl_category_fk
FOREIGN KEY (Parent_category_id)
REFERENCES public.tbl_category (category_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_event ADD CONSTRAINT tbl_visibility_tbl_event_fk
FOREIGN KEY (visibility_id)
REFERENCES public.tbl_visibility (visibility_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_promoter ADD CONSTRAINT tbl_status_promoter_tbl_promoter_fk
FOREIGN KEY (status_promoter_id)
REFERENCES public.tbl_status_promoter (status_promoter_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_event ADD CONSTRAINT tbl_status_event_tbl_event_fk
FOREIGN KEY (status_event_id)
REFERENCES public.tbl_status_event (status_event_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_item ADD CONSTRAINT tbl_currency_tbl_item_fk
FOREIGN KEY (currency_id)
REFERENCES public.tbl_currency (currency_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_event_user ADD CONSTRAINT tbl_permission_tbl_event_user_fk
FOREIGN KEY (permission_id)
REFERENCES public.tbl_permission (permission_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_event_user ADD CONSTRAINT tbl_event_tbl_event_user_fk
FOREIGN KEY (event_id)
REFERENCES public.tbl_event (event_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_item ADD CONSTRAINT tbl_event_tbl_item_fk
FOREIGN KEY (event_id)
REFERENCES public.tbl_event (event_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_event_keyword ADD CONSTRAINT tbl_event_tbl_event_keyword_fk
FOREIGN KEY (event_id)
REFERENCES public.tbl_event (event_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_enrollment ADD CONSTRAINT tbl_event_tbl_enrollment_fk
FOREIGN KEY (event_id)
REFERENCES public.tbl_event (event_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_photo ADD CONSTRAINT tbl_event_tbl_photo_fk
FOREIGN KEY (event_id)
REFERENCES public.tbl_event (event_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_event_user ADD CONSTRAINT tbl_user_tbl_event_user_fk
FOREIGN KEY (user_id)
REFERENCES public.tbl_user (user_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_promoter ADD CONSTRAINT tbl_user_tbl_promoter_fk
FOREIGN KEY (user_id)
REFERENCES public.tbl_user (user_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_phone ADD CONSTRAINT tbl_user_tbl_phone_fk
FOREIGN KEY (user_id)
REFERENCES public.tbl_user (user_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_enrollment ADD CONSTRAINT tbl_user_tbl_enrollment_fk
FOREIGN KEY (user_id)
REFERENCES public.tbl_user (user_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tbl_item ADD CONSTRAINT tbl_promoter_tbl_item_fk
FOREIGN KEY (promoter_id)
REFERENCES public.tbl_promoter (promoter_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;