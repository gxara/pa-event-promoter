const express = require("express");

const { createFBUser, checkIfAuthenticated, checkIfAuthenticatedNonBlock } = require("../controllers/authcontroller")
const { getUsers, getUserById, userValidated, getUser, getUserEvents, getUserSubscriptions, editUser } = require("../controllers/users");
const { getEvent, getEventById, createEvent, editEvent, insertPhotoEvent, getEventByName, getEventByCategory, getEventDescription, getEventItems, editItem, createItem, removeItem} = require("../controllers/events");
const { enrollmentEvent, removeEnrollment } = require("../controllers/relationship");

const router = express.Router();


router.get("/user", getUsers); // Não valida token // Endpoints que não devem ser usados em produção (somente teste)
router.get("/user/oi", checkIfAuthenticated, userValidated); // Endpoints que não devem ser usados em produção (somente teste)

router.get("/user/info", checkIfAuthenticated, getUser);
router.get("/user/myEvents", checkIfAuthenticated, getUserEvents);
router.get("/user/mySubscriptions", checkIfAuthenticated, getUserSubscriptions);

router.get("/user/:id", getUserById); // Não valida token // Endpoints que não devem ser usados em produção (somente teste)

router.get("/_health_check", (req, res) => res.send("I'm alive!"));  // Não valida token // Endpoints que não devem ser usados em produção (somente teste)]

router.get("/event", checkIfAuthenticatedNonBlock, getEvent);
router.get("/event/:id", checkIfAuthenticatedNonBlock, getEventById);
router.get("/event/:id/desc", checkIfAuthenticatedNonBlock, getEventDescription);

router.get("/event/:id/item", checkIfAuthenticatedNonBlock, getEventItems);

router.get("/event/name/:name", checkIfAuthenticatedNonBlock, getEventByName);
router.get("/event/category/:category", checkIfAuthenticatedNonBlock, getEventByCategory);

// Métodos POST
router.post("/user/register", createFBUser);
router.post("/event", checkIfAuthenticated, createEvent);
router.post("/event/:id/item", checkIfAuthenticated, createItem);
router.post("/addEventPhoto", checkIfAuthenticated, insertPhotoEvent);
router.post("/enrollUser", checkIfAuthenticated, enrollmentEvent); // É necessário criar uma tabela de convite para eventos privados

// Métodos PATCH
router.patch("/user", checkIfAuthenticated, editUser);
router.patch("/event/:id", checkIfAuthenticated, editEvent);
router.patch("/event/item/:item_id", checkIfAuthenticated, editItem);

// Métodos DELETE
router.delete("/event/item/:item_id", checkIfAuthenticated, removeItem);

router.all("*", (req, res) => res.status(404).send("Route not found"));

module.exports = router;
