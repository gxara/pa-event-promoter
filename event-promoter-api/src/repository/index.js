const { Pool } = require("pg");
const { checkIfAuthenticated } = require("../controllers/authcontroller")

const pool = new Pool({
  user: "pa-admin",
  // host: "172.2.222.11", // Mudar para env var (tem que ser db quando for rodar com container)
  // host: "localhost", // Mudar para env var (tem que ser db quando for rodar com container)
  host: "db", // Mudar para env var (tem que ser db quando for rodar com container)
  database: "pa-event-promoter",
  password: "!tes@jg41EA",
  port: 5432,
});

class UsersRepository {
  static async getUsers() {
    return pool
      .query("SELECT * FROM tbl_user;")
      .then((response) => response.rows);
  }

  static async getUser(uid) {
    const text = 
      "SELECT * FROM tbl_user where user_uid = $1";
      
    const values = [uid];

    return pool.query(text, values).then((res) => res.rows[0]);
  }

  static insertUser(name, surname, birthdate, email, uid) {
    const text =
      "INSERT INTO public.tbl_user (user_name, user_surname, user_birthdate, user_email, user_uid)  \
    VALUES ($1, $2, $3, $4, $5);";

    const values = [name, surname, birthdate, email, uid];

    return pool.query(text, values).then((res) => {
      console.log(res);
    });
  }

  static async updateUser(user_id,body) {

    const setStatement = Object.keys(body).map((key,i) => {return `${key} = \$${i+1}`});

    const text =
      `UPDATE tbl_user
      SET 
        ${setStatement}
      WHERE user_id = ${user_id};`

    const values = Object.values(body);

    return pool.query(text, values).then((res) => {
      console.log(res);
    });

  }

  static async insertPhoneUser(phone, uid) {
    const text =
      "INSERT INTO public.tbl_phone (phone_number, user_id)  \
  VALUES ($1, (select tu.user_id from tbl_user tu where tu.user_uid = $2));";
    console.log("QUERY: " + text)

    const values = [phone, uid];

    return pool.query(text, values).then((res) => {
      console.log(res);
    });
  }

  static async getUserEvents(user_uid) {
    const text = 
    "with events_owned as( \
      select teu.event_id \
      from tbl_event_user  teu \
      left join tbl_user tu on tu.user_id = teu.user_id \
      where teu.permission_id = 1 and tu.user_uid = $1\
    ),\
    events_enrolled as(\
      select * \
      from  tbl_event te \
      where event_id in (select * from events_owned)\
    )\
    select \
      te.*, \
      tbl_photo.photo_path, \
      tc.category_description, \
      tv.visibility_description, \
      tse.status_event_description, \
      (select count(*) from tbl_enrollment where (enrollment_id in (select max(enrollment_id) from tbl_enrollment group by user_id, event_id)) and (enrollment_option_id=1 or enrollment_option_id=2) and (event_id = te.event_id)) as num_participantes \
    from events_enrolled te \
    LEFT JOIN tbl_photo ON te.event_id = tbl_photo.event_id \
    inner join tbl_category tc on tc.category_id = te.category_id \
    inner join tbl_visibility tv on tv.visibility_id = te.visibility_id \
    inner join tbl_status_event tse on tse.status_event_id = te.status_event_id \
    WHERE (tbl_photo.photo_position = 0 or tbl_photo.photo_position is null);";

    const values = [user_uid];

    return pool.query(text, values).then((res) => res.rows);
  }

  static async getUserSubscriptions(user_uid) {
    const text = 
    "with events_owned as(\
      select te.event_id\
      from tbl_enrollment te \
      left join tbl_user tu on tu.user_id = te.user_id  \
      where tu.user_uid = $1\
    ),\
    events_enrolled as(\
      select * \
      from  tbl_event te \
      where event_id in (select * from events_owned)\
    )\
    select \
      te.*, \
      tbl_photo.photo_path, \
      tc.category_description, \
      tv.visibility_description, \
      tse.status_event_description, \
      (select count(*) from tbl_enrollment where (enrollment_id in (select max(enrollment_id) from tbl_enrollment group by user_id, event_id)) and (enrollment_option_id=1 or enrollment_option_id=2) and (event_id = te.event_id)) as num_participantes \
    from events_enrolled te \
    LEFT JOIN tbl_photo ON te.event_id = tbl_photo.event_id \
    inner join tbl_category tc on tc.category_id = te.category_id \
    inner join tbl_visibility tv on tv.visibility_id = te.visibility_id \
    inner join tbl_status_event tse on tse.status_event_id = te.status_event_id \
    WHERE (tbl_photo.photo_position = 0 or tbl_photo.photo_position is null);";

    const values = [user_uid];

    return pool.query(text, values).then((res) => res.rows);
  }
}

class RelationshipRepository {
  static async enrollUser(user_id, event_id, enrollment_id_option) {
    const text =
      "INSERT INTO public.tbl_enrollment (enrollment_created_at, user_id, event_id, enrollment_option_id) \
    VALUES ((now() at time zone 'America/Sao_Paulo'), $1, $2, $3);";

    console.log(text);
    const values = [
      parseInt(user_id),
      parseInt(event_id),
      parseInt(enrollment_id_option),
    ];

    console.log(values);
    return pool.query(text, values).then((res) => {
      console.log(res);
    });
  }

  static async unenrollUser(user_id, event_id) {
    const text = 
      "DELETE FROM tbl_enrollment\
      WHERE user_id = $1 and event_id = $2";

    const values = [user_id, event_id];

    return pool.query(text, values).then((res) => {
      console.log(res);
    });
  }
}

class EventsRepository {
  static async getEvents(uid) {
    const text =  "select te.*, tbl_photo.photo_path, tc.category_description, tv.visibility_description, tse.status_event_description, \
      (select count(*) from tbl_enrollment where (enrollment_id in (select max(enrollment_id) from tbl_enrollment group by user_id, event_id)) and (enrollment_option_id=1 or enrollment_option_id=2) and (event_id = te.event_id)) as num_participantes \
      from tbl_event te \
      LEFT JOIN tbl_photo ON te.event_id = tbl_photo.event_id \
      inner join tbl_category tc on tc.category_id = te.category_id \
      inner join tbl_visibility tv on tv.visibility_id = te.visibility_id \
      inner join tbl_status_event tse on tse.status_event_id = te.status_event_id \
      WHERE (tbl_photo.photo_position = 0 or tbl_photo.photo_position is null) \
      and (tv.visibility_description like 'Public' or \
    	te.event_id in (select event_id from tbl_event_user teu left join tbl_user tu on tu.user_id = teu.user_id where tu.user_uid is not null and tu.user_uid = $1));";
    
    const values = [uid];

    return pool.query(text, values).then((response) => response.rows);
  }

  static async getEventKeywords(event_id) {
    const text = 
      "with allKeywords as (\
        select keyword_id \
        from tbl_event te \
        left join tbl_event_keyword tek on te.event_id = tek.event_id\
        where te.event_id = $1\
      ),\
      keywordsDescriptions as (\
        select keyword_description\
        from allKeywords te\
        inner join tbl_keyword tek on te.keyword_id = tek.keyword_id\
      ) \
      select * from keywordsDescriptions";
      
    const values = [event_id];

    return pool.query(text, values).then((response) => response.rows);
  }

  static async getEventItems(event_id, uid) {
    const text = 
      "with allItems as (\
        select *\
        from tbl_event te\
        inner join tbl_item ti on ti.event_id = te.event_id\
        where te.event_id = $1 and (te.visibility_id = 1 or $1 in (select teu.event_id from tbl_event_user teu left join tbl_user tu on teu.user_id = tu.user_id where tu.user_uid is not null and tu.user_uid = $2))\
      ),\
      allItemsCur as (\
        select *  \
        from allItems ai\
        inner join tbl_currency tc on ai.currency_id = tc.currency_id\
      ),\
      ItemCurPromo as (\
        select *\
        from allItemsCur cr\
        inner join tbl_promoter tp on cr.promoter_id = tp.promoter_id\
      ),\
      allDone as (\
        select icr.item_name, icr.item_description, icr.item_price, icr.currency_description, icr.promoter_name, icr.promoter_description, tsp.status_promoter_description\
        from ItemCurPromo icr\
        inner join tbl_status_promoter tsp on icr.status_promoter_id  = tsp.status_promoter_id\
      )\
      select * from allDone";
    
    const values = [event_id, uid];

    return pool.query(text, values).then((response) => response.rows);
  }

  static async getEventPromoters(event_id) {
    const text = 
      "with allEnrolledUsers as (\
        select user_id\
        from tbl_enrollment te \
        left join tbl_event tek on te.event_id = tek.event_id\
        where te.event_id = $1\
      ),\
      allPromoters as (\
        select *\
        from allEnrolledUsers te \
        inner join tbl_promoter tek on te.user_id = tek.user_id\
      )\
      select * from allPromoters";

    const values = [event_id];

    return pool.query(text,values).then((response) => response.rows);
  }

  static async getEventsByName(event_name, uid) {
    const text =
      "select te.*, tbl_photo.photo_path, tc.category_description, tv.visibility_description, tse.status_event_description, \
    (select count(*) from tbl_enrollment where (enrollment_id in (select max(enrollment_id) from tbl_enrollment group by user_id, event_id)) and (enrollment_option_id=1 or enrollment_option_id=2) and (event_id = te.event_id)) as num_participantes \
    from tbl_event te \
    LEFT JOIN tbl_photo ON te.event_id = tbl_photo.event_id \
    inner join tbl_category tc on tc.category_id = te.category_id \
    inner join tbl_visibility tv on tv.visibility_id = te.visibility_id \
    inner join tbl_status_event tse on tse.status_event_id = te.status_event_id \
    WHERE (tbl_photo.photo_position = 0 or tbl_photo.photo_position is null) and lower(te.event_name) like lower($1) \
    and (tv.visibility_description like 'Public' or \
    	te.event_id in (select event_id from tbl_event_user teu left join tbl_user tu on tu.user_id = teu.user_id where tu.user_uid is not null and tu.user_uid = $2));";

    const values = ["%" + event_name + "%", uid];

    return pool.query(text, values).then((response) => response.rows);
  }

  static async checkPermissionTo(user_id, event_id, action) {
    var text = "select teu.event_id from tbl_event_user teu where teu.user_id = $1 and teu.event_id = $2";
    
    if (action == 'insertItem' || action == 'editItem' || action == 'removeItem') {
      text = text + ' and teu.permission_id in (1)';
    } else if (action == 'insertPhotoEvent') {
      text = text + ' and teu.permission_id in (1)';
    } else if (action == 'editEvent') {
      text = text + ' and teu.permission_id in (1)';
    }

    text = text + ';'
    const values = [parseInt(user_id), parseInt(event_id)];
    return pool.query(text, values).then((response) => response.rows);
  }

  static async getEventsByCategory(event_category, uid) {
    const text = 
      "with recursive categories as (\
        select te.*, 1 as relative_depth\
        from tbl_category te\
        where lower(te.category_description) like lower($1)\
        union all \
          select tc.*, cat.relative_depth + 1\
          from tbl_category tc, categories cat \
          where tc.parent_category_id = cat.category_id \
      ) \
      select * from tbl_event te where te.category_id in \
        (select category_id from categories) and \
        (te.visibility_id = 1 or te.event_id in \
          (select teu.event_id from tbl_event_user teu left join tbl_user tu on teu.user_id = tu.user_id where tu.user_uid is not null and tu.user_uid = $2))";

		const values = ["%" + event_category + "%", uid];

		return pool.query(text, values).then((response) => response.rows);
  }

  static async insertPhoto(photo_path, event_id, photo_position) {
    const text =
      "INSERT INTO public.tbl_photo (photo_path, photo_created_at, event_id, photo_position) \
  VALUES ($1, (now() at time zone 'America/Sao_Paulo'), $2, $3);";

    const values = [photo_path, parseInt(event_id), parseInt(photo_position)];

    return pool.query(text, values).then((res) => {
      console.log(res);
    });
  }

  static async insertEventToUser(user_id, event_id) {
    const text =
      "INSERT INTO public.tbl_event_user (event_user_created_at, user_id, event_id, permission_id) \
  VALUES ((now() at time zone 'America/Sao_Paulo'), $1, $2, 1);";

    const values = [parseInt(user_id), parseInt(event_id)];

    return pool.query(text, values).then((res) => {
      console.log(res);
    });
  }

  static async insertEvent(
    event_name,
    event_description,
    event_date,
    event_address_patio,
    event_address_number,
    event_address_complement,
    event_address_postcode,
    status_event_id,
    visibility_id,
    category_id
  ) {
    const text =
      "INSERT INTO tbl_event ( \
      event_name, \
      event_description, \
      event_date, \
      event_created_at,\
      event_address_patio, \
      event_address_number, \
      event_address_complement, \
      event_address_postcode, \
      status_event_id, \
      visibility_id, \
      category_id) \
  VALUES ($1, $2, $3, (now() at time zone 'America/Sao_Paulo'), $4, $5, $6, $7, $8, $9, $10) returning event_id;";

    const values = [
      event_name,
      event_description,
      event_date,
      event_address_patio,
      parseInt(event_address_number),
      event_address_complement,
      event_address_postcode,
      parseInt(status_event_id),
      parseInt(visibility_id),
      parseInt(category_id),
    ];

    return pool.query(text,values).then((response) => response.rows[0]['event_id']);
  }

  static async updateEvent(event_id,body) {

    const setStatement = Object.keys(body).map((key,i) => {return `${key} = \$${i+1}`});

    const text =
      `UPDATE tbl_event
      SET 
        ${setStatement}
      WHERE event_id = ${event_id};`

    const values = Object.values(body);

    return pool.query(text, values).then((res) => {
      console.log(res);
    });

  }

  static async deleteEvent() {}

  static async insertItem(
    name,
    description,
    price,
    promoter_id,
    event_id,
    currency
  ) {
    const text = 
      "INSERT INTO tbl_item\
        (item_name,\
        item_description,\
        item_price,\
        promoter_id,\
        event_id,\
        currency_id)\
      VALUES ($1,$2,$3,$4,$5,$6);";

    const values = [
      name,
      description,
      price,
      promoter_id,
      event_id,
      currency
    ]

    return pool.query(text,values).then((res) => {
      console.log(res);
    });
  }

  static async getEventFromItem(item_id) {
    const text = "SELECT event_id FROM public.tbl_item where item_id = $1";
    const values = [parseInt(item_id)];

    return pool.query(text, values).then((res) => res.rows[0]['event_id']);
  }

  static async updateItem(item_id, body) {
    const setStatement = Object.keys(body).map((key,i) => {return `${key} = \$${i+1}`});
    const text =
      `UPDATE tbl_item
      SET 
        ${setStatement}
      WHERE item_id = ${item_id}`; 
    
    const values = Object.values(body);

    return pool.query(text, values).then((res) => {
      console.log(res);
    });
  }

  static async deleteItem(item_id) {
    const text = 
      "DELETE FROM tbl_item\
      WHERE item_id = $1;";
    
    const values = [item_id];
    
    return pool.query(text, values).then((res) => {
      console.log(res);
    });
  }
}

module.exports = { UsersRepository, RelationshipRepository, EventsRepository };
