const { UsersRepository, EventsRepository } = require("../../repository/index");

const getUsers = async (req, res) => {
  const allUsers = await UsersRepository.getUsers();
  return res.status(200).json(allUsers);
};

const getUser = async (req, res) => {
  const user = await UsersRepository.getUser(req.authId);
  return res.status(200).json(user);
};

const getUserById = async (req, res) => {
  // Melhoria: fazer a consulta SQL com a condicional direto no BD
  const allUsers = await UsersRepository.getUsers();
  const requestedId = req.params.id;
  const requestedUser = allUsers.filter((u) => u.user_id == requestedId);
  return res.status(200).json(requestedUser);
};

const userValidated = async (req, res) => {
  return res.status(200).send({ msg: 'You are authorized!', uid:req.authId, id:req.userId });
}

const getUserEvents = async (req, res) => {
  const user_uid = req.authId;
  const allEvents = await UsersRepository.getUserEvents(user_uid);
  return res.status(200).json(allEvents);
}

const getUserSubscriptions = async (req, res) => {
  const user_uid = req.authId;
  const allEvents = await UsersRepository.getUserSubscriptions(user_uid);
  return res.status(200).json(allEvents);
}

const editUser = async (req, res) => {
  const user_id = req.userId;
  await UsersRepository.updateUser(
    user_id, req.body
  );
  return res.status(200).json({
    message: "User updated successfully"
  });
}

module.exports = { getUsers, getUserById, userValidated, getUser, getUserEvents, getUserSubscriptions, editUser };