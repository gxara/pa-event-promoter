var admin = require("firebase-admin");

// var serviceAccount = require("/usr/app/pa-event-promoter-google.json");

var serviceAccount = {
  "type": "service_account",
  "project_id": "pa-event-promoter",
  "private_key_id": "7a4d77c7649c909bd25412a41ba6d9ec1df89d8f",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCN9Xwp1Xvaoh3X\nmmihjHxbGDVCI3fSdsfumBofIrjEVc1/jsYK66J90GXNdFWG9p89RKw6EOnCOk/0\nj+I0w56NCQflrTodKxKR2DxtxTe33z+hdxjSa44M+87peEr7pf/WhLaP6OuCVdgq\nQ0uc1/Fu91YJbOMNPYhLvPsKk/q40H9uxjtXupNPUyPq325ZIChE4ZEgXxooZy30\nQVFmH8mfop+mjuycExRJ4Oj7J8pZccJc00DMT+vJNEj7uQMOmFkW4fAW/PxkVUeO\nNsPdxtZYZ9xARwVm6h1f6ugmiN6ue/cWQnihKjdvif23mMaayuAKNay2DirzE2ND\nL1G89hpJAgMBAAECggEAEDkoz8MaXMRq3VPudzU4lP8xlEsSC4G68ZUnub0xw8ZR\nLi22i6hIMVFaq7M1fwS6SA82u4q+nDgmdmWRzW6PCmvTdOJqIiE6Ww6M7A0+Loub\nQwGlsDkjH8Vl7R/r5Hx2aMv77SW08c02X1tszbzkAPF6lKiBU5G0/NWDvxQQSC89\nfaHdIXboTmWJWlDVYVWCllL5jhO5/ohoNRQqOGKx040A8mG2s15o2RzENNN63ZVl\nRvVGc93xzK5wAiraPOMr+e1uLPL8D2Ugv6M3HXbjuSP1sJ2NQwigRfqAqIY0Hq4B\nCZ1palaGXBgSFw2V5zL0FJaC3LGLDSLkYhnMdD1SzQKBgQDA4l6KHghL4tFtAIkR\nZ/w8n/qO9m7M8doB0tjdS0UsHxb52FZzXcH9Ge+L7VNQ30fFoFVg8Ix8fb0k42A/\nuKrFqYrMX54fqwJIVzSgYFbaOoUZDj4seWSxnLPnkzxJ7E13pE+HTeGawXCiVO0O\njGaMiMmV/jVMxHcKD1fibNQt7wKBgQC8aSz6eWVhgSg0XNLzIAFjucR0x8kg30lY\nMfv0Y1StfXyIyNq/gRE+2lht6z0cgRf9hZFSCfwMhrEk9o71lvtvXQW9spGEbEOC\nH149lKJn67hCXHSiQ8MhAC13iKO7BZTJZtvzGZD7ebViY2B6W0TBuAuMPwonRVHi\nvqox9TVzRwKBgD1O9pvSJfLw+n46IFc/BSbD9uFAJUHRUekoerQEO81laGkrwXbU\nA1gOyVxhqAzZm7eOWfUBILZlRRyXhT1b1jQTysFhVyJbudMCnQCo/pJI7YtkQpDi\nWQY3d9uQqiYZDRqNSJYKRD5rZADFG8B3EhVzWRfrAEgRXoD9FYMr8llTAoGAfH5i\nof6k5pCZKEBNgyiawnJ1RVTdqULhC827Vfhsc1Z/e0J8iB2Yb5eG5tqRPsxuNa6U\nH0lJql01n97rB4wEB8nHiVj8niuLT+e6NaaIyXIQBolg5/cucBfV8y3uQh2l+o5k\nt3UlP4VkPOa9URwNipM5zim5Y8mCGfkyLGCCOPECgYAKE5460Tlg1n/TeX+svmN4\n063L8CO2HnJx3x6yGR0AZJHm3FEOM67iy0d9nbW57Sr/BeHR2ZPKjeffCXMtgFd1\nxnWy+CVAT+0EcIxDhaMyGD9JaOTk7J1loqPBKCAxuiY8YgCaacKt/GEEUz+Q1fWi\nrgUhAStTrSYjIERL3K8iCQ==\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-ln35k@pa-event-promoter.iam.gserviceaccount.com",
  "client_id": "106462464909500122049",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-ln35k%40pa-event-promoter.iam.gserviceaccount.com"
}


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

module.exports = { admin }
