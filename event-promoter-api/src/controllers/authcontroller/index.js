const { admin } = require('./firebase-service')
const { UsersRepository } = require("../../repository/index")

const createFBUser = async (req, res) => {
    const {
        email,
        password,
        firstName,
        lastName,
        birthdate,
    } = req.body;

    const user = await admin.auth().createUser({
      email,
      password,
      displayName: `${firstName} ${lastName}`
    })
    .then((userRecord) => {
        // See the UserRecord reference doc for the contents of userRecord.
        console.log('Successfully created new user:', userRecord.uid);
        const insertUserDB = UsersRepository.insertUser(
            firstName,
            lastName,
            birthdate,
            email,
            userRecord.uid,
        );

        console.log('Insert User DB:', insertUserDB);
    })
    .catch((error) => {
        console.log('Error creating new user:', error);
        return res.status(401);
    });

    // return res.send(user);
    return res.status(200).json(user);
    
}

const getAuthToken = async (req, res, next) => {
    if (
        req.headers.authorization &&
        req.headers.authorization.split(' ')[0] === 'Bearer'
    ) {
        req.authToken = req.headers.authorization.split(' ')[1];
    } else {
        req.authToken = null;
    }
    next();
};


const checkIfAuthenticated = (req, res, next) => {
    getAuthToken(req, res, async () => {
        try {
            const { authToken } = req;
            const userInfo = await admin
            .auth()
            .verifyIdToken(authToken);
            req.authId = userInfo.uid;
            
            const user = await UsersRepository.getUser(userInfo.uid);
            req.userId = user['user_id'];

            return next();
        } catch (e) {
            return res
            .status(401)
            .send({ error: 'You are not authorized to make this request' });
        }
        });
};

const checkIfAuthenticatedNonBlock = (req, res, next) => {
    getAuthToken(req, res, async () => {
        try {
            const { authToken } = req;
            const userInfo = await admin
            .auth()
            .verifyIdToken(authToken);
            req.authId = userInfo.uid;

            const user = await UsersRepository.getUser(userInfo.uid);
            req.userId = user['user_id'];

            return next();
        } catch (e) {
            req.authId = null;
            req.userId = null;
            return next();
        }
        });
};
  

module.exports = { createFBUser, checkIfAuthenticated, checkIfAuthenticatedNonBlock };

