const { getUser, createUser } = require("./users");
const { getEvent, createEvent } = require("./events");

module.exports = {
  getUser,
  createUser,
  getEvent,
  createEvent,
};
