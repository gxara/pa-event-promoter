const { EventsRepository } = require("../../repository/index");

const createEvent = async (req, res) => {
  const payload = req.body;
  const event_id = await EventsRepository.insertEvent(
    payload.event_name,
    payload.event_description,
    payload.event_date,
    payload.event_address_patio,
    payload.event_address_number,
    payload.event_address_complement,
    payload.event_address_postcode,
    payload.status_event_id,
    payload.visibility_id,
    payload.category_id,
    );

  console.log(event_id);
  
  await EventsRepository.insertEventToUser(req.userId, event_id);
  
  return res.status(201).json({
    message: "Event inserted with success",
  });
};

const editEvent = async (req, res) => {
  const event_id = req.params.id;

  const permission_event = await EventsRepository.checkPermissionTo(req.userId, event_id, 'editEvent');

  if (permission_event.length == 0) {
    return res.status(401).json({
      message: "Not allowed!"
    }); 
  }

  await EventsRepository.updateEvent(
    event_id, req.body
  );
  return res.status(200).json({
    message: "Event updated successfully"
  });
}

const insertPhotoEvent = async (req, res) => {
  const payload = req.query;

  const permission_event = await EventsRepository.checkPermissionTo(req.userId, payload.event, 'insertPhotoEvent');

  if (permission_event.length == 0) {
    return res.status(401).json({
      message: "Not allowed!"
    }); 
  }


  await EventsRepository.insertPhoto(
    payload.path,
    payload.event,
    payload.position,
    );
  return res.status(201).json({
    message: "Photo added with success",
  });
};


const getEvent = async (req, res) => {
  const allEvents = await EventsRepository.getEvents(req.authId);
  return res.status(200).json(allEvents);
};

const getEventByName = async (req, res) => {
  const payload = req.params;
  const allEventsByName = await EventsRepository.getEventsByName(payload.name, req.authId);
  return res.status(200).json(allEventsByName);
};

const getEventByCategory = async (req, res) => {
  const payload = req.params;
  const allEventsByCategory = await EventsRepository.getEventsByCategory(payload.category, req.authId);
  return res.status(200).json(allEventsByCategory);
}

const getEventById = async (req, res) => {
  // Melhoria: fazer a consulta SQL com a condicional direto no BD
  const allEvents = await EventsRepository.getEvents(req.authId);
  const requestedId = req.params.id;
  const requestedEvent = allEvents.filter((u) => u.event_id == requestedId);
  return res.status(200).json(requestedEvent);
};

const getEventDescription = async (req, res) => {
  const requestedId = req.params.id;
  const allEvents = await EventsRepository.getEvents(req.authId);
  const allEventAttributes = allEvents.filter((u) => u.event_id == requestedId)[0];

  if (typeof allEventAttributes === "undefined") {
    return res.status(200).json({});
  }
  
  const allKeywords = await EventsRepository.getEventKeywords(requestedId);
  const allItems = await EventsRepository.getEventItems(requestedId);
  const allPromoters = await EventsRepository.getEventPromoters(requestedId);
  const allDescription = {
    ...allEventAttributes, 
    keywords_description:allKeywords.map((k) => k['keyword_description']), 
    items:allItems, 
    promoters:allPromoters
  };
  return res.status(200).json(allDescription);
};

const getEventItems = async (req, res) => {
  const requestedId = req.params.id;
  const allItems = await EventsRepository.getEventItems(requestedId, req.authId);
  return res.status(200).json(allItems);
}

const createItem = async (req, res) => {
  const event_id = req.params.id;
  const payload = req.body;

  const permission_event = await EventsRepository.checkPermissionTo(req.userId, event_id, 'insertItem');

  if (permission_event.length == 0) {
    return res.status(401).json({
      message: "Not allowed!"
    }); 
  }

  await EventsRepository.insertItem(
    payload.item_name,
    payload.item_description,
    payload.item_price,
    payload.promoter_id,
    event_id,
    payload.currency_id
  );
  return res.status(201).json({
    message: "Item created with success"
  });
}

const editItem = async (req, res) => {
  const item_id = req.params.item_id;
  const event_id = await EventsRepository.getEventFromItem(item_id);
  const permission_event = await EventsRepository.checkPermissionTo(req.userId, event_id, 'editItem');
  if (permission_event.length == 0) {
    return res.status(401).json({
      message: "Not allowed!"
    }); 
  }

  await EventsRepository.updateItem(item_id, req.body);
  return res.status(200).json({
    message:"Item updated successfully"
  });
}

const removeItem = async (req, res) => {
  const item_id = req.params.item_id;

  const event_id = await EventsRepository.getEventFromItem(item_id);
  
  const permission_event = await EventsRepository.checkPermissionTo(req.userId, event_id, 'removeItem');
  if (permission_event.length == 0) {
    return res.status(401).json({
      message: "Not allowed!"
    }); 
  }

  await EventsRepository.deleteItem(item_id);
  return res.status(200).json({
    message:"Item deleted successfully"
  });
}

module.exports = { getEvent, getEventById, createEvent, editEvent, insertPhotoEvent, getEventByName, getEventByCategory, getEventDescription, getEventItems, createItem, editItem, removeItem };
