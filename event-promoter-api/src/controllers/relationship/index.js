const { RelationshipRepository } = require("../../repository/index");

const enrollmentEvent = async (req, res) => {
  const payload = req.body;
  console.log(payload);
  try {
    await RelationshipRepository.enrollUser(
      req.userId,
      payload.event,
      payload.enrollment_option
    );
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      message: "There is an error. Try again.",
    });
  }
  return res.status(200).json({
    message: "User enrolled with success",
  });
};

const removeEnrollment = async (req, res) => {
  const payload = req.body;
  try {
    await RelationshipRepository.unenrollUser(
      payload.user,
      payload.event
    );
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      message: "There is an error. Try again.",
    });
  }
  return res.status(200).json({
    message: "Unenrolled successfully"
  });
}

module.exports = { enrollmentEvent, removeEnrollment };
