const express = require("express");
const routes = require("./routes");
const cors = require("cors");

const app = express();

app.use(cors());

app.use(
  express.json({
    strict: false,
  })
);

const port = 5000;

app.use(routes);

app.listen(port, () => {
  console.log(`Event Promoter App listening at http://localhost:${port}`);
});

module.exports = app;
